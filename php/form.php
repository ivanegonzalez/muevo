<?php
    $to = "marketing-01@trasladatransfers.com.ar";
    $name = $_POST["_FirstName"];
    $email = $_POST["_Email"];
    $phone = $_POST["_Phone1"];
    $body = 'Nombre: ' . $name  . "\r\n "
        . 'Teléfono: ' . $phone . "\r\n "
        . 'Email: ' . $email . "\r\n ";
    
    if ($_POST['subject'] == 'Contacto') {
        $subject = "Contacto";
        $empresa = $_POST["_Empresa"];
        $body .= 'Empresa: ' . $empresa . "\r\n ";
    } else {
        $subject = $_POST["subject"];
        $origen = $_POST["_Origen"];
        $destino = $_POST["_Destino"];
        $tipodeServicio = $_POST["_TipodeServicio"];
        $metodoDePago = $_POST["_MetodoDePago"];
        $seguroPropio = $_POST["_SeguroPropio"];
        $body .= 'Origen: ' . $origen . "\r\n "
        . 'Destino: ' . $destino . "\r\n "
        . 'Tipo de servicio: ' . $tipodeServicio . "\r\n "
        . 'Método de pago: ' . $metodoDePago . "\r\n "
        . 'Seguro propio: ' . $seguroPropio . "\r\n ";
    }
    
    $message = $_POST["_DatosEspecificos"];
    $body .= 'Mensaje: ' . $message;

    mail($to, $subject, $body, $headers); 
    header("Location: http://www.muevo.com.ar/empresa/gracias.html");